Python with MySQL Sample [![Build Status](https://apibeta.shippable.com/projects/537665ba4f325a2600369f3e/badge/master)](https://beta.shippable.com/projects/537665ba4f325a2600369f3e)
===================
This sample is built for Shippable, a docker based continuous integration and deployment platform.
[![Run Status](https://apibeta.shippable.com/projects/56e1139dc77dae78a8f477f7/badge?branch=master)](https://beta.shippable.com/projects/56e1139dc77dae78a8f477f7)